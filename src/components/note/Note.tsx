import React from 'react'

export interface INoteProps {
    key: number,
    title: string,
    body: string,
    time: string,
    done: boolean
}

export const Note = (props: INoteProps) => {
    {
        return (
            <div>
                <h2>{props.title}</h2>
                <p>Description: {props.body}</p>
                <p>When should I do it: {props.time}</p>
            </div>
        )
    }
    
}
