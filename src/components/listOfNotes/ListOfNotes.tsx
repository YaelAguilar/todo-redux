import React from 'react'
import { Note } from '../note/Note'
import { useSelector } from 'react-redux'
import { INoteProps } from '../note'

export interface EnumListofNotesStyling {
    style: "dark"|"white"
}

interface RootState {
    notes: INoteProps[]
}


// thanks to redux, we don't have to pass the contents of each post into the list
// it's just fetched from the store
export const ListOfNotes = ({style} : EnumListofNotesStyling) => {
    const notes = useSelector((state:RootState) => state.notes)

return (
    <div>
    {notes.map((note: INoteProps) => (
        <Note
            key={note.key}
            title={note.title}
            body={note.body}
            time={note.time}
            done={note.done}
        />
        ))
    }
    </div>
)

    
}