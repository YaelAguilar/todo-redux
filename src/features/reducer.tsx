const initState = {
    notes: [
        {key: 1, title: "Make cookies with chocolate", body: "The recipe is in the fridge", time: "tomorrow morning", done: false},
        {key: 2, title: "Clean the house", body: "Every single room should be cleaned", time: "Next week", done: false},
        {key: 3, title: "Buy milk", body: "It's cheaper in the closest supermarket", time: "today", done: true},
        {key: 4, title: "take the dog for a walk", body: "In the neighborhood", time: "each afternoon", done: false},
    ]
}


export const rootReducer = (state = initState, _action: any) => {
    return state;
}

export default rootReducer