import React from 'react';
import {Note} from './components/note/Note';
import logo from './logo.svg';
import './App.css';
import { ListOfNotes } from './components/listOfNotes';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <ListOfNotes style={"white"}/>
      </header>
    </div>
  );
}

export default App;
